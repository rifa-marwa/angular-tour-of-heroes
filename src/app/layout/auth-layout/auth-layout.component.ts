import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd } from '@angular/router';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { filter } from 'rxjs';
import { ɵKeyEventsPlugin } from '@angular/platform-browser';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.css']
})
export class AuthLayoutComponent implements OnInit {

  notResetPassword = true;
  subscribeRoute: any;

  constructor(private route:Router, private location: Location) { }

  ngOnInit(): void {

    this.route.events.pipe(filter(event => event instanceof NavigationEnd))
    .subscribe((event) => {
      this.subscribeRoute = event;
    }
    );

    if(this.route.routerState.snapshot.url === "/reset-password-notification"){
      this.notResetPassword = false;
    }
  }

}
