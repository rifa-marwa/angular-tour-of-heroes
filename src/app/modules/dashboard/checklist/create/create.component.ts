import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChecklistService } from 'src/app/services/checklist/checklist.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  data : any;  
  errorMessage : string = '';
  itemName = new FormControl('');
  isSubmitted = false;
  submitNotification : string = '';

  constructor(
    private checklistApi:ChecklistService,
    public dialogRef: MatDialogRef<CreateComponent>
  ) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.data = {
      name : this.itemName.value
    }

    this.checklistApi.createChecklist(this.data).subscribe({
      next: (result => {
        console.log(result);
        this.isSubmitted = true;
        this.submitNotification = 'To do berhasil ditambahkan'
        window.location.reload();
      }),
      error: (error => {
        this.isSubmitted = true;
        this.errorMessage = error.message;
        this.errorMessage = this.errorMessage[0].toUpperCase() + this.errorMessage.substr(1).toLowerCase();
      })
    })
  }

}
