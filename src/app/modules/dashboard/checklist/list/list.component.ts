import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ChecklistService } from 'src/app/services/checklist/checklist.service';
import { ListChecklistItemComponent } from '../../checklist-item/list-checklist-item/list-checklist-item.component';
import { CreateComponent } from '../create/create.component';
import { map, Observable, of } from 'rxjs';
import { ChecklistItemService } from 'src/app/services/checklist-item/checklist-item.service';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  checklistData:any;
  checklistDone: any[] = [];
  checklistItemData: any;
  checklistToDo: any[] = [];
  errorMessage : any;
  isMaxTabletWidth: boolean = false;

  constructor(
    private cdr: ChangeDetectorRef,
    private checklistApi:ChecklistService,
    private checklistItemApi: ChecklistItemService,
    public dialog: MatDialog,
    private observer: BreakpointObserver,
  ) { 
  }

  ngAfterViewInit() {
    this.observer.observe(['(max-width: 600px']).subscribe((result) => {
      if (result.matches) {
        this.isMaxTabletWidth = true;
      } else {
        this.isMaxTabletWidth = false;
      }
    })
    this.cdr.detectChanges();
  }

  ngOnInit(): void {
    this.getAllChecklist();
  }

  deleteChecklist(id: number){
    this.checklistApi.deleteChecklist(id).subscribe({
      next: (() => {
        window.location.reload();
      })
    })
  }

  drop(event: CdkDragDrop<any[]>){
    if(event.previousContainer === event.container){
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem (
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      )

      const recentDropItem = event.container.data[event.currentIndex]

      this.checklistItemApi.getAllChecklistItem(recentDropItem.id).subscribe({
        next: (result => {
          this.checklistItemData = result;

          if(recentDropItem.checklistCompletionStatus === false){
            this.checklistItemData.data.forEach((item:any) => {
              if (item.itemCompletionStatus === false){
                this.checklistItemApi.editStatusChecklistItem('', item.id, recentDropItem.id).subscribe({
                  next: ((result) => { recentDropItem.checklistCompletionStatus = true })
                })
              }
            }) 
          } 
          else {
            this.checklistItemData.data.forEach((item:any) => {
              this.checklistItemApi.editStatusChecklistItem('', item.id, recentDropItem.id).subscribe({
                next: ((result) => { recentDropItem.checklistCompletionStatus = false })
              })
            })
          } 
        }) 
      })

    }
  }

  getAllChecklist(){
    this.checklistApi.getAllChecklist().subscribe(result => {
      this.checklistData = result;
      const itemsObservable = of(this.checklistData.data);

      const itemsTodo = itemsObservable.pipe(
        map((item: any[]) => item.filter((item) => item.checklistCompletionStatus === false))
      );

      const itemsDone = itemsObservable.pipe(
        map((item: any[]) => item.filter((item) => item.checklistCompletionStatus === true))
      );

      itemsTodo.subscribe((data:any) => {
        this.checklistToDo.push(data);
      })

      itemsDone.subscribe((data:any) => {
        this.checklistDone.push(data)
      })

    })
  }

  openAddDialog(){
    this.dialog.open(CreateComponent);
  }

  openDetailDialog(id: number, name: string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '50em';
    dialogConfig.height = '90%';

    dialogConfig.data = {
        id: id,
        name: name
    };

    this.dialog.open(ListChecklistItemComponent, dialogConfig);
    
  }

}
