import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateChecklistItemComponent } from './update-checklist-item.component';

describe('UpdateChecklistItemComponent', () => {
  let component: UpdateChecklistItemComponent;
  let fixture: ComponentFixture<UpdateChecklistItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateChecklistItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UpdateChecklistItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
