import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChecklistItemService } from 'src/app/services/checklist-item/checklist-item.service';
import { CreateChecklistItemComponent } from '../create-checklist-item/create-checklist-item.component';
import { UpdateChecklistItemComponent } from '../update-checklist-item/update-checklist-item.component';

@Component({
  selector: 'app-list-checklist-item',
  templateUrl: './list-checklist-item.component.html',
  styleUrls: ['./list-checklist-item.component.css']
})
export class ListChecklistItemComponent implements OnInit {

  checklistItemData: any;
  checklistItemToDo : any[] = [];
  checklistItemDone: any[] = [];
  
  constructor(
    private checklistItemApi: ChecklistItemService,
    public dialog: MatDialog,
    @Inject (MAT_DIALOG_DATA) public data:any
  ) { }

  ngOnInit(): void {
    this.getChecklistItem();
    console.log(this.data)
  }

  addItemDialog(id:number){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
        id: id
    };
    this.dialog.open(CreateChecklistItemComponent, dialogConfig)
  }

  closeDialog(){
    window.location.reload();
  }

  deleteChecklistItem(checklistItemId: number){
    console.log(this.data.id)
    this.checklistItemApi.deleteChecklistItem(this.data.id, checklistItemId).subscribe({
      next: (() => {
        window.location.reload();
      })
    })
  }

  drop(event: CdkDragDrop<any[]>){
    if(event.previousContainer === event.container){
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem (
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      )

      const recentDropItem = event.container.data[event.currentIndex]

      this.checklistItemApi.editStatusChecklistItem('', recentDropItem.id, this.data.id).subscribe({
        next: ((result) => {
          console.log(result)
        })
      })
    }
  }

  getChecklistItem(){
    this.checklistItemApi.getAllChecklistItem(this.data.id).subscribe({
      next: (result => {
        this.checklistItemData = result;
        this.checklistItemData = this.checklistItemData.data;
        console.log(this.checklistItemData)

        for (let data of this.checklistItemData){
          if (data.itemCompletionStatus === true){
            this.checklistItemDone.push(data)
          } else {
            this.checklistItemToDo.push(data)
          }
        }
      }),
      error : (error => {
        console.log(error)
      })
    })
  }

  openDetailDialog(id:number, name: string, status: boolean){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
        checklistItemId: id,
        checklistId: this.data.id,
        name: name,
        status: status
    };

    this.dialog.open(UpdateChecklistItemComponent, dialogConfig);
  }

}
