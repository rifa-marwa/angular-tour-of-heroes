import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { CreateComponent } from './checklist/create/create.component';
import { ListComponent } from './checklist/list/list.component';
import { ComponentsModule } from 'src/app/components/components.module';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatInput, MatInputModule } from '@angular/material/input';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateChecklistItemComponent } from './checklist-item/create-checklist-item/create-checklist-item.component';
import { ListChecklistItemComponent } from './checklist-item/list-checklist-item/list-checklist-item.component';
import { UpdateChecklistItemComponent } from './checklist-item/update-checklist-item/update-checklist-item.component';

@NgModule({
  declarations: [
    CreateComponent,
    ListComponent,
    CreateChecklistItemComponent,
    ListChecklistItemComponent,
    UpdateChecklistItemComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ComponentsModule,
    MatFormFieldModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    DragDropModule,
    ReactiveFormsModule
  ]
})
export class DashboardModule { }
