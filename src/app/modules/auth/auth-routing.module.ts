import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForgotPasswordFormComponent } from './forgot-password-form/forgot-password-form.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { ResetPasswordFormComponent } from './reset-password-form/reset-password-form.component';
import { ResetPasswordNotificationComponent } from './reset-password-notification/reset-password-notification.component';
import { SignInFormComponent } from './sign-in-form/sign-in-form.component';

const routes: Routes = [
  {
    path: 'sign-in',
    component: SignInFormComponent
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordFormComponent
  },
  {
    path: 'reset-password',
    component: ResetPasswordFormComponent
  },
  {
    path: 'reset-password-notification',
    component: ResetPasswordNotificationComponent
  },
  {
    path: 'register',
    component: RegisterFormComponent
  },
  {
    path: '',
    redirectTo: '/sign-in',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
