import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  errorMessage : any;
  isError = false;
  isRegistrationSuccess = false;
  hide = true;
  registerForm : FormGroup;
  response : any;

  constructor(
    private authApi: AuthService,
    private formBuilder: FormBuilder
  ) { 
    this.registerForm = this.formBuilder.group({
      username: new FormControl('', [Validators.required]),
      email : new FormControl('', [Validators.required, Validators.email]),
      password : new FormControl('', [Validators.required, Validators.minLength(8)])
    })
  }

  ngOnInit(): void {}

  get username(){
    return this.registerForm.get('username')
  }

  get email(){
    return this.registerForm.get('email')
  }

  get password(){
    return this.registerForm.get('password')
  }

  getErrorMessage(form: string) {
    switch(form){
      case 'username' : 
        if (this.username?.hasError('required')){
          return 'Username cannot be empty';
        }
        break;
      case 'email' : 
        if (this.email?.hasError('required')){
          return 'Email cannot be empty';
        }
        return this.email?.hasError('email') ? 'Not a valid email' : '';
      case 'password' :
        if (this.password?.hasError('required')){
          return 'Password cannot be empty'
        }
        return this.password?.hasError('minlength') ? 'Password must have at least 8 characters' : '';
      default: 
        return ''
    }
    return ''
  }

  onSubmit() : void {
    const data = this.registerForm.value;
    if (data.username && data.email && data.password) {
      this.authApi.register(this.registerForm.value).subscribe(
        {
          next: ((result:any) => {
            this.response = result;
            this.isRegistrationSuccess = true;
            console.log(this.response)
          }),
          error: ((error:any) => {
            this.errorMessage = error.message
            this.errorMessage = this.errorMessage[0].toUpperCase() + this.errorMessage.substr(1).toLowerCase();
            this.isError = true;
          })
        }
      )
    }

  }

}
