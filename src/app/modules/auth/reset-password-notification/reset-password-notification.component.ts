import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reset-password-notification',
  templateUrl: './reset-password-notification.component.html',
  styleUrls: ['./reset-password-notification.component.css']
})
export class ResetPasswordNotificationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
