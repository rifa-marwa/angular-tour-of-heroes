import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChecklistService {

  endpoint: string = 'checklist';

  constructor(private http: HttpClient) { }

  getAllChecklist(){
    return this.http.get(this.endpoint);
  }

  createChecklist(data:any){
    return this.http.post(this.endpoint, data);
  }

  deleteChecklist(id:number){
    return this.http.delete(`${this.endpoint}/${id}`);
  }
}
