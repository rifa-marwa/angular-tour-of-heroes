import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { AlertComponent } from './alert/alert.component';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
  declarations: [
    CardComponent,
    AlertComponent
  ],
  imports: [
    CommonModule,
    MatIconModule
  ],
  exports: [
    CardComponent, 
    CommonModule
  ]
})
export class ComponentsModule { }
