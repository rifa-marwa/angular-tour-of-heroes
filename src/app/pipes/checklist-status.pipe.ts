import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'checklist-status'
})

export class ChecklistStatusPipe implements PipeTransform {
    transform(value: any, ...args: any[]) {
        if (value === false) {
            return 'undone'
        } else if (value === true) {
            return 'done'
        }
        throw new Error('Method not implemented.');
    }

}